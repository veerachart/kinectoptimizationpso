from pyswarm import pso
from math import pi, sin, cos, tan
import numpy as np
import matplotlib.pyplot as plt
import timeit
from pylab import *
import sys,os
import time
import matplotlib.patches as mpatches

pathname = os.path.dirname(sys.argv[0])

N = 0
MAX_N = 5
fopt = 0
swarmSize = 20
iterLimit = 10
searchLimX = [-1.5,1.5]
searchLimY = [-1.5,1.5]
searchLimZ = [0.5,1.5]
searchStep = 0.1

zPlots = 3
zSteps = (searchLimZ[1]-searchLimZ[0])/(zPlots-1.)
zLayers = np.arange(searchLimZ[0],searchLimZ[1]+zSteps/2.0,zSteps)

def state_to_coord(x_state):
    if x_state < 0 or x_state > 20:
        return
    if x_state < 5:
        x = -2.5
        y = x_state-2.5
    elif x_state < 10:
        x = x_state-7.5
        y = 2.5
    elif x_state < 15:
        x = 2.5
        y = 12.5-x_state
    else:
        x = 17.5-x_state
        y = -2.5
    z = 2
    return x,y,z

def optimization_f(x_state):
    # Change from state
    kPosX = []
    kPosY = []
    kPosZ = []
    kRoll = []
    kPitch = []
    kYaw = []
    kTransformation = []
    for i in range(N):
        x, y, z = state_to_coord(x_state[i*3+0])
        kPosX.append(x)
        kPosY.append(y)
        kPosZ.append(z)
        kRoll.append(0.0*pi/180)
        kPitch.append(x_state[i*3+1]*pi/180)
        if x_state[i*3] - 0 < 0.01 or 20 - x_state[i*3] < 0.01:         # Lower left corner
            kYaw.append(x_state[i*3+2]*pi/180 + pi/4)
        elif x_state[i*3] < 5:                                          # Left side
            kYaw.append(x_state[i*3+2]*pi/180)
        elif abs(x_state[i*3] - 5.0) < 0.01:                            # Upper left corner
            kYaw.append(x_state[i*3+2]*pi/180 - pi/4)
        elif x_state[i*3] < 10:                                         # Top side
            kYaw.append(x_state[i*3+2]*pi/180 - pi/2)
        elif abs(x_state[i*3] - 10.0) < 0.01:                           # Upper right corner
            kYaw.append(x_state[i*3+2]*pi/180 - 3*pi/4)
        elif x_state[i*3] < 15:                                         # Right side
            kYaw.append(x_state[i*3+2]*pi/180 + pi)
        elif abs(x_state[i*3] - 15.0) < 0.01:                           # Lower right corner
            kYaw.append(x_state[i*3+2]*pi/180 + 3*pi/4)
        else:                                                           # Bottom side
            kYaw.append(x_state[i*3+2]*pi/180 + pi/2)

        kTransformation.append(np.array([[cos(kPitch[i])*cos(kYaw[i]),  sin(kRoll[i])*sin(kPitch[i])*cos(kYaw[i]) - cos(kRoll[i])*sin(kYaw[i]), cos(kRoll[i])*sin(kPitch[i])*cos(kYaw[i]) + sin(kRoll[i])*sin(kYaw[i]), kPosX[i]],
                                         [cos(kPitch[i])*sin(kYaw[i]),  sin(kRoll[i])*sin(kPitch[i])*sin(kYaw[i]) + cos(kRoll[i])*cos(kYaw[i]), cos(kRoll[i])*sin(kPitch[i])*sin(kYaw[i]) - sin(kRoll[i])*cos(kYaw[i]), kPosY[i]],
                                         [-sin(kPitch[i]),              sin(kRoll[i])*cos(kPitch[i]),                                           cos(kRoll[i])*cos(kPitch[i]),                                           kPosZ[i]],
                                         [0,                            0,                                                                      0,                                                                      1]]))

    f_sum = 0

    for z in np.arange(searchLimZ[0], searchLimZ[1]+0.5/2.0, 0.5):
        for x in np.arange(searchLimX[0], searchLimX[1]+searchStep/2.0, searchStep):
            for y in np.arange(searchLimY[0], searchLimY[1]+searchStep/2.0, searchStep):
                f = 0
                for T in kTransformation:
                    X = np.array([[x],
                                  [y],
                                  [z],
                                  [1]])
                    Xi = np.dot(np.linalg.inv(T), X)
                    xi = Xi[0,0]
                    yi = Xi[1,0]
                    zi = Xi[2,0]

                    if xi < 0.45 or xi > 4.5:
                        continue
                    if (yi < -(tan(28*pi/180))*xi) or (yi > (tan(28*pi/180))*xi):
                        continue
                    if (zi < -(tan(21*pi/180))*xi) or (zi > (tan(21*pi/180))*xi):
                        continue

                    # The point x,y,z can be viewed by the Kinect
                    f += 1

                if f == 0:
                    return 0

                f_sum += f
    return -f_sum

while fopt >= 0 and N < MAX_N:
    N += 1
    print "N =",N
    lb = [0,  10, -30]*N
    ub = [20, 30, 30]*N

    start_time = timeit.default_timer()
    xopt, fopt = pso(optimization_f, lb, ub, swarmsize=swarmSize, maxiter=iterLimit)
    elapsed = timeit.default_timer() - start_time

    print xopt, fopt
    print ""

color = ['g','y','r','b']
markers = ['.','*','^','s']

kPosX = []
kPosY = []
kPosZ = []
kRoll = []
kPitch = []
kYaw = []
kTransformation = []
for i in range(N):
    x, y, z = state_to_coord(xopt[i*3+0])
    kPosX.append(x)
    kPosY.append(y)
    kPosZ.append(z)
    kRoll.append(0.0*pi/180)
    kPitch.append(xopt[i*3+1]*pi/180)
    if xopt[i*3] - 0 < 0.01 or 20 - xopt[i*3] < 0.01:         # Lower left corner
        kYaw.append(xopt[i*3+2]*pi/180 + pi/4)
    elif xopt[i*3] < 5:                                       # Left side
        kYaw.append(xopt[i*3+2]*pi/180)
    elif abs(xopt[i*3] - 5.0) < 0.01:                         # Upper left corner
        kYaw.append(xopt[i*3+2]*pi/180 - pi/4)
    elif xopt[i*3] < 10:                                      # Top side
        kYaw.append(xopt[i*3+2]*pi/180 - pi/2)
    elif abs(xopt[i*3] - 10.0) < 0.01:                        # Upper right corner
        kYaw.append(xopt[i*3+2]*pi/180 - 3*pi/4)
    elif xopt[i*3] < 15:                                      # Right side
        kYaw.append(xopt[i*3+2]*pi/180 + pi)
    elif abs(xopt[i*3] - 15.0) < 0.01:                        # Lower right corner
        kYaw.append(xopt[i*3+2]*pi/180 + 3*pi/4)
    else:                                                     # Bottom side
        kYaw.append(xopt[i*3+2]*pi/180 + pi/2)

    kTransformation.append(np.array([[cos(kPitch[i])*cos(kYaw[i]),  sin(kRoll[i])*sin(kPitch[i])*cos(kYaw[i]) - cos(kRoll[i])*sin(kYaw[i]), cos(kRoll[i])*sin(kPitch[i])*cos(kYaw[i]) + sin(kRoll[i])*sin(kYaw[i]), kPosX[i]],
                                     [cos(kPitch[i])*sin(kYaw[i]),  sin(kRoll[i])*sin(kPitch[i])*sin(kYaw[i]) + cos(kRoll[i])*cos(kYaw[i]), cos(kRoll[i])*sin(kPitch[i])*sin(kYaw[i]) - sin(kRoll[i])*cos(kYaw[i]), kPosY[i]],
                                     [-sin(kPitch[i]),              sin(kRoll[i])*cos(kPitch[i]),                                           cos(kRoll[i])*cos(kPitch[i]),                                           kPosZ[i]],
                                     [0,                            0,                                                                      0,                                                                      1]]))

lineColor = ['k','b','#3C0F00','y']

figs = []
axs  = []
for i in range(zPlots):
    figs.append(plt.figure())
    axs.append(figs[i].add_subplot(111))
    axis('equal')
    axis([-2.5,2.5,-2.5,2.5])
    plt.gca().set_aspect('equal',adjustable='box')
    axs[i].set_title('z = ' + str(zLayers[i]) + ' m')
    axs[i].set_xlabel('x (m)')
    axs[i].set_ylabel('y (m)')
    box = axs[i].get_position()
    axs[i].set_position([box.x0-0.1, box.y0, box.width, box.height])
    for f in range(1,N+1):
        axs[i].plot(-100,-100,markers[f-1],c=color[f-1],markeredgewidth=0,label='$f='+str(f)+'$')
    plt.legend(loc='center left', numpoints=1, bbox_to_anchor=(1.0,0.5))

# TODO add legends for Kinects
for j in range(zPlots):
    for i in range(N):
        axs[j].scatter(kPosX[i],kPosY[i],s=50,c=lineColor[i],marker='s',linewidths=0)
        axs[j].arrow(kPosX[i],kPosY[i],0.3*cos(kYaw[i]),0.3*sin(kYaw[i]),head_width=0.05,head_length=0.1,fc=lineColor[i],ec=lineColor[i])

for z in zLayers:
    plotIndex = list(zLayers).index(z)
    for x in np.arange(searchLimX[0], searchLimX[1]+searchStep/2.0, searchStep):
        for y in np.arange(searchLimY[0], searchLimY[1]+searchStep/2.0, searchStep):
            f = 0
            for T in kTransformation:
                X = np.array([[x],
                              [y],
                              [z],
                              [1]])
                Xi = np.dot(np.linalg.inv(T), X)
                xi = Xi[0,0]
                yi = Xi[1,0]
                zi = Xi[2,0]

                if xi < 0.45 or xi > 4.5:
                    continue
                if (yi < -(tan(28*pi/180))*xi) or (yi > (tan(28*pi/180))*xi):
                    continue
                if (zi < -(tan(21*pi/180))*xi) or (zi > (tan(21*pi/180))*xi):
                    continue

                # The point x,y,z can be viewed by the Kinect
                f += 1
            axs[plotIndex].scatter(x,y,c=color[f-1],marker=markers[f-1],linewidths=0)

plotStep = searchStep/10.
lineThres = 0.005

for z in zLayers:
    plotIndex = list(zLayers).index(z)
    for x in np.arange(-2.5, 2.5+plotStep/2.0, plotStep):
        for y in np.arange(-2.5, 2.5+plotStep/2.0, plotStep):
            i = 0
            for T in kTransformation:
                X = np.array([[x],
                              [y],
                              [z],
                              [1]])
                Xi = np.dot(np.linalg.inv(T), X)
                xi = Xi[0,0]
                yi = Xi[1,0]
                zi = Xi[2,0]

                if xi < 0.45 - lineThres or xi > 4.5 + lineThres:
                    i += 1
                    continue
                if (yi < -(tan(28*pi/180))*xi - lineThres) or (yi > (tan(28*pi/180))*xi + lineThres):
                    i += 1
                    continue
                if (zi < -(tan(21*pi/180))*xi - lineThres) or (zi > (tan(21*pi/180))*xi + lineThres):
                    i += 1
                    continue
                if (xi > 0.45 + lineThres) and (xi < 4.5 - lineThres) and (yi > -(tan(28*pi/180))*xi + lineThres) and (yi < (tan(28*pi/180))*xi - lineThres) and (zi > -(tan(21*pi/180))*xi + lineThres) and (zi < (tan(21*pi/180))*xi - lineThres):
                    i += 1
                    continue

                axs[plotIndex].scatter(x,y,c=lineColor[i],marker='.',s=10,linewidths=0)
                i += 1

# TODO add options to save the log or just display
if not os.path.isdir(pathname+'/logdata'):
   os.makedirs(pathname+'/logdata')
date_time = time.strftime('%Y%m%d_%H%M')
if not os.path.isdir(pathname+'/logdata/'+date_time):
   os.makedirs(pathname+'/logdata/'+date_time)
logfile_name = pathname+'/logdata/'+date_time+'/log.txt'
figs_name = []
for i in range(zPlots):
    figs_name.append(pathname+ '/logdata/' +date_time+'/z%02d' % (5*(i+1)))
logfile = open(logfile_name, 'wb')
logfile.write('N = ' +str(N)+'\n')
logfile.write('Swarm size = ' +str(swarmSize)+'\n')
logfile.write('Max iterations = '+str(iterLimit)+'\n')
for i in range(N):
   logfile.write('Kinect ' + str(i+1) + ' at (' +  str(kPosX[i]) + ', ' + str(kPosY[i]) + ', ' + str(kPosZ[i]) + '), ('
                 + str(kRoll[i]) + ', ' + str(xopt[i*3+1]) + ', ' + str(kYaw[i]*180/pi) + ').\n')
logfile.write('Best coverage = ' + str(-fopt) + '\n')
logfile.write('Which is equal to ' + str(-fopt/(((searchLimX[1]-searchLimX[0])/searchStep + 1) * ((searchLimY[1]-searchLimY[0])/searchStep + 1) * ((searchLimZ[1]-searchLimZ[0])/0.5 + 1))) + ' Kinects per point.\n')
logfile.write('Elapsed time for optimization = ' + str(elapsed) + ' seconds\n')
logfile.close()

for i in range(zPlots):
    figs[i].show()
    figs[i].savefig(figs_name[i]+'.eps', dpi=600, facecolor='w', edgecolor='w',
           orientation='portrait', papertype='a4', format='eps',
           transparent=True, bbox_inches=None, pad_inches=0.1,
           frameon=None)
    figs[i].savefig(figs_name[i]+'.png', dpi=600, facecolor='w', edgecolor='w',
           orientation='portrait', papertype='a4', format='png',
           transparent=True, bbox_inches=None, pad_inches=0.1,
           frameon=None)
print 'Elapsed time:', elapsed
print '*************************'
for i in range(N):
    print 'Kinect', i+1, 'at (', kPosX[i], ',', kPosY[i], ',', kPosZ[i], '), (', kRoll[i], ',', xopt[i*3+1], ',', kYaw[i]*180/pi, ').'
